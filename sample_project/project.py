import flow
from flow.project import FlowProject

@FlowProject.label
def complete(job):
    return 'tmax' in job.document

@FlowProject.operation
@FlowProject.post(complete)
def calculate(job):
    import numpy as np
    g = 9.81
    roots = np.roots([-g/2, np.sin(job.sp.theta), 0])
    tmax = roots[roots != 0][0]
    job.doc.tmax = tmax

if __name__ == "__main__":
    FlowProject().main()
import signac
import numpy as np

project = signac.get_project('sample_project')
for v in [1, 2, 3]:
    for theta in np.round(np.linspace(0, 3.14/2, 5), 2):
        project.open_job({"v": v, "theta": theta}).init()

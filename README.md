# About

This folder contains the various examples and useful scripts that needed for the
SciPy paper and presentation.
Currently in use are:

* sample\_project: The very simple project used for figures 2 and 3 in the
  paper.
* add\_data.py: The script used to add data to the sample\_project in the middle
  of the workflow. It is stored here so that it doesn't show up during `ls`
  calls in the example notebook.
* signac - SciPy.ipynb: The main notebook containing an index of the other notebooks and examples to be used for the presentation.
* projectile: The more involved visualization project used in the presentation.
* integration: Examples of how to integrate signac with other tools that are somewhat related to the problem.

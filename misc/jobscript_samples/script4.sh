#!/bin/bash
#PBS -N my_project
#PBS -l nodes=16:ppn=8,walltime=12:00:00
#PBS -A ${MYACCOUNT}
#PBS -q ${MYQUEUE}
 
cd ${WORKING_DIRECTORY}
 
mpirun -n 16 ./myscript.sh

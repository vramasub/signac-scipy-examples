#!/bin/bash
#SBATCH -J myproject
#SBATCH -N 16
#SBATCH -A ${MYACCOUNT}
#SBATCH -p ${QUEUE}
#SBATCH --ntasks-per-node 8
#SBATCH -t 12:00:00

cd ${WORKING_DIRECTORY}

mpirun -n 16 ./myscript.sh

#!/bin/bash
#PBS -N my_project
#PBS -l nodes=16,walltime=12:00:00
 
cd ${WORKING_DIRECTORY}
 
mpirun -n 16 ./myscript.sh
